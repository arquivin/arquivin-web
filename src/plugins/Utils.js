import Vue from 'vue';

var UtilsPlugin = {
  clone: function(obj) {
    return Object.assign({}, obj);
  },
};

export default function(Vue) {
  Vue.utils = UtilsPlugin;

  Object.defineProperties(Vue.prototype, {
    $utils: {
      get: function() {
        return Vue.utils;
      }
    }
  })


}
