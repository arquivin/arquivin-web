import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currentUser: {},
    currentCompany: {},
    currentCompanyConfig: {},
    userEdit: {},
    accessGroup: {},
    project: {},
    showMenu: false
  },
  mutations: {
    setCurrentUser: function(state, user) {
      state.currentUser = user;
    },
    clearCurrentUser: function(state) {
      state.currentUser = {};
    },
    setUserEdit: function(state, user) {
      state.userEdit = user;
    },
    clearUserEdit: function(state) {
      state.userEdit = {};
    },
    setAccessGroupEdit: function(state, accessGroup) {
      state.accessGroup = accessGroup;
    },
    clearAccessGroupEdit: function(state) {
      state.accessGroup = {};
    },
    setProjectEdit: function(state, project) {
      state.project = project;
    },
    clearProjectEdit: function(state) {
      state.project = {};
    },
    setCurrentCompany: function(state, company) {
      state.currentCompany = company;
    },
    clearCurrentCompany: function(state) {
      state.currentCompany = {};
    },
    setShowMenu: function(state, showMenu) {
      state.showMenu = showMenu;
    },
    clearShowMenu: function(state) {
      state.showMenu = false;
    },
    setCurrentCompanyConfig: function(state, companyConfig) {
      state.currentCompanyConfig = companyConfig;
    },
    clearCurrentCompanyConfig: function(state) {
      state.currentCompanyConfig = {};
    }
  }
})
