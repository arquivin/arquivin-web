import Vue from 'vue';

var AuthPlugin = {
  setToken: function(token) {
    localStorage.setItem('authToken', token);
  },
  setExpiration: function(expiration) {
    localStorage.setItem('expirationToken', (new Date().getTime() + parseFloat(expiration)));
  },
  removeToken: function() {
    localStorage.removeItem('authToken');
  },
  getToken: function() {
    var token = localStorage.getItem('authToken');
    if (!token) {
      return null;
    }
    return token;
  },
  hasToken: function() {
    if (this.getToken()) {
      return true;
    }
    return false;
  },
  isValid() {
    var expiration = localStorage.getItem('expirationToken');
    if (!this.getToken() || (new Date().getTime() > expiration)) {
        console.log('Token invalido ou expirado!');
        this.removeToken();
      return false;
    }

    return true;
  }
};

export default function(Vue) {
  Vue.auth = AuthPlugin;

  Object.defineProperties(Vue.prototype, {
    $auth: {
      get: function() {
        return Vue.auth;
      }
    }
  })


}
