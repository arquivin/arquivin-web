import Vue from 'vue';

var LoadingScreenPlugin = {
  show: function() {
    $.LoadingOverlay("show", {
      image: "",
      fontawesome: "fa fa-spinner fa-spin"
    });
  },
  hide: function() {
    $.LoadingOverlay("hide");
  },
  appendAjaxCall: function() {
    $(document).ajaxStart(function() {
      $.LoadingOverlay("show", {
        image: "",
        fontawesome: "fa fa-spinner fa-spin"
      });
    });
    $(document).ajaxStop(function() {
      $.LoadingOverlay("hide");
    });
  }
};

export default function(Vue) {
  Vue.loadingScreen = LoadingScreenPlugin;

  Object.defineProperties(Vue.prototype, {
    $loadingScreen: {
      get: function() {
        return Vue.loadingScreen;
      }
    }
  })


}
