import Vue from 'vue';

import Snotify, {
  SnotifyPosition
} from 'vue-snotify';

Vue.use(Snotify);

var NotifyPlugin = {
  addMessage: function(Message, Type) {
    this.createMessage("", Message, Type);
  },
  addSuccessMessage: function(Message) {
    this.createMessage('Sucesso', Message, 'success');
  },
  addInfoMessage: function(Message) {
    this.createMessage('Info', Message, 'info');
  },
  addErrorMessage: function(Message) {
    this.createMessage('Erro', Message, 'error');
  },
  addWarnMessage: function(Message) {
    this.createMessage('Atenção', Message, 'warning');
  },
  createMessage: function(Title, Message, Type) {
    // Vue.prototype.$snotify.success(Message, 'Erro')
    Vue.prototype.$snotify.create({
      title: Title,
      body: Message,
      config: {
        position: SnotifyPosition.rightTop,
        type: Type,
        timeout: 5000
      }
    })
  }
};

export default function(Vue) {
  Vue.notify = NotifyPlugin;

  Object.defineProperties(Vue.prototype, {
    $notify: {
      get: function() {
        return Vue.notify;
      }
    }
  })


}
