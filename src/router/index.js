import Vue from 'vue'
import Router from 'vue-router'
import Auth from '@/components/auth/Auth'
import Login from '@/components/auth/Login'
import Recover from '@/components/auth/Recover'
import ResetPassword from '@/components/auth/ResetPassword'
import Invite from '@/components/invite/Invite'
import Home from '@/components/home/Home'
import Profile from '@/components/home/Profile'
import Users from '@/components/users/Users.vue'
import UserDetail from '@/components/users/UserDetail.vue'
import AccessGroups from '@/components/accessgroups/AccessGroups.vue'
import Projects from '@/components/project/Projects'
import ProjectTabs from '@/components/project/ProjectTabs'
import ProjectDetail from '@/components/project/ProjectDetail'
import AccessGroupDetail from '@/components/accessgroups/AccessGroupDetail.vue';
import CompanyDetail from '@/components/company/CompanyDetail.vue';

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      redirect: '/auth/login'
    },
    {
      path: '/auth',
      redirect: '/auth/login'
    },
    {
      path: '/invite',
      redirect: '/auth/invite'
    },
    {
      path: '/auth',
      component: Auth,
      meta: {
        requiresGuest: true
      },
      children: [{
          path: 'login',
          component: Login
        },
        {
          path: 'recover',
          component: Recover
        },
        {
          path: 'resetPassword',
          component: ResetPassword
        },
        {
          path: 'invite',
          component: Invite
        }
      ]
    },
    {
      path: '/home',
      component: Home,
      children: [{
          path: 'users',
          component: Users
        },
        {
          path: 'accessGroups',
          component: AccessGroups
        },
        {
          path: 'projects',
          component: Projects
        },
        {
          path: 'projectTabs',
          component: ProjectTabs
        },
        {
          path: 'projectDetail',
          component: ProjectDetail
        },
        {
          path: 'userDetail',
          component: UserDetail
        },
        {
          path: 'accessGroupDetail',
          component: AccessGroupDetail
        },
        {
          path: 'companyDetail',
          component: CompanyDetail
        },
        {
          path: 'profile',
          component: Profile
        }
      ]
    }
  ]
})
