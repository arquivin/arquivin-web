// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
//import BootstrapVue from 'bootstrap-vue'
import VueResource from 'vue-resource'
import Auth from './plugins/Auth.js'
import Notify from './plugins/Notify.js'
import VueTheMask from 'vue-the-mask'
import Utils from './plugins/Utils.js'
import Store from './plugins/Store.js'
import LoadingScreen from './plugins/LoadingScreen.js'
import Vue2Filters from 'vue2-filters'
import VueMoment from 'vue-moment'
import SuiVue from 'semantic-ui-vue';
import 'semantic-ui-css/semantic.min.css';
// import VeeValidate from 'vee-validate';
import VeeValidate, { Validator } from 'vee-validate';

import VeeValidateMessagesBR from "vee-validate/dist/locale/pt_BR";
Validator.localize('pt_BR', VeeValidateMessagesBR);

import VueSweetalert2 from 'vue-sweetalert2';
import Vuex from 'vuex'
import VueMq from 'vue-mq'
Vue.use(VueMq, {
  breakpoints: {
    mobile: 450,
    tablet: 900,
    laptop: 1250,
    desktop: Infinity,
  }
})

Vue.use(VueResource);
Vue.use(Auth);
Vue.use(Notify);
Vue.use(VueTheMask);
Vue.use(Utils);
Vue.use(LoadingScreen);
Vue.use(Vue2Filters);
Vue.use(VueMoment);
Vue.use(VeeValidate);
Vue.use(SuiVue);
Vue.use(VueSweetalert2);
Vue.use(Vuex)


Vue.prototype.isEditorRole = function() {
  console.log('#############', this.$store.state);
}

window.hub = new Vue;

global.Vue = Vue;

$.extend(true, $.fn.dataTable.defaults, {
  destroy: true,
  pageLength: 10,
  language: {
      "sEmptyTable": "Nenhum registro encontrado",
      "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
      "sInfoFiltered": "(Filtrados de _MAX_ registros)",
      "sInfoPostFix": "",
      "sInfoThousands": ".",
      "sLengthMenu": "_MENU_ resultados por página",
      "sLoadingRecords": "Carregando...",
      "sProcessing": "Processando...",
      "sZeroRecords": "Nenhum registro encontrado",
      "sSearch": "Pesquisar",
      "oPaginate": {
          "sNext": "Próximo",
          "sPrevious": "Anterior",
          "sFirst": "Primeiro",
          "sLast": "Último"
      },
      "oAria": {
          "sSortAscending": ": Ordenar colunas de forma ascendente",
          "sSortDescending": ": Ordenar colunas de forma descendente"
      }
  }
});

Vue.config.productionTip = false;

Vue.http.interceptors.push(function(request, next) {
  this.$loadingScreen.show();
  console.log("Interceptor for url " + request.url[0]);
  // modify request
  if (request.url[0] === '/') {
    // Se iniciar com barra eh requisicao para a api

    // Tratamento para buscar o menu no static
    var index = request.url.indexOf('/static/menu/');
    console.log(request.url+" - "+index);
    if(index === -1){
      request.url = process.env.URL_API + request.url;
    }

    var token = Vue.auth.getToken();
    if (token) {
      request.headers.set('Authorization', 'Bearer ' + token);
    }
  }

  // continue to next interceptor
  next(function(response) {
    this.$loadingScreen.hide();
    // modify response
    if (response.status === 400) {
      var messages = response.body;
      messages.forEach(function(m) {
        Vue.notify.addMessage(m.msg, m.type.toLowerCase());
      });
    } else if (response.status === 401 || response.status === 403) {
      request.url = process.env.URL_API + request.url;
      console.log('request.url '+request.url);
      var token = Vue.auth.getToken();
      if (token) {
        request.headers.set('Authorization', 'Bearer ' + token);
      }
    }

  });
});

router.beforeEach(function(to, from, next) {
  var queryParams = null;
  if (to.query.token) {
    localStorage.setItem('token', to.query.token);
  } else if (from.query.token) {
    localStorage.setItem('token', from.query.token);
  }
  if (to.matched.some(function(record) {
      return record.meta.requiresGuest
    }) && Vue.auth.isValid()) {
    next({
      path: '/home/projects',
      query: queryParams
    })
  } else if (!Vue.auth.hasToken() && !(to.path === process.env.LOGIN_PATH) && !(to.path === process.env.RECOVER_PATH) && !(to.path === process.env.RESET_PWD_PATH) && !(to.path === process.env.INVITE_PATH)) {
    next({
      path: process.env.LOGIN_PATH,
      query: queryParams
    })
  } else {
    next({
      query: queryParams
    });
  }
})


/* eslint-disable no-new */
new Vue({
  el: '#app',
  store: Store,
  router,
  template: '<App/>',
  components: {
    App
  }
})
