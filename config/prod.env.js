module.exports = {
  NODE_ENV: '"production"',
  URL_API: '"http://api.arquiv.in"',
  REST_CHECK_TOKEN: '"/validateToken"',
  LOGIN_PATH: '"/auth/login"',
  RECOVER_PATH: '"/auth/recover"',
  RESET_PWD_PATH: '"/auth/resetPassword"',
  INVITE_PATH: '"/auth/invite"'
}
