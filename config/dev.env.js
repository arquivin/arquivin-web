var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  URL_FRONT: '"http://localhost:8080"',
  URL_API: '"http://localhost:9090/arquivin-api"',
  REST_CHECK_TOKEN: '"/validateToken"',
  LOGIN_PATH: '"/auth/login"',
  RECOVER_PATH: '"/auth/recover"',
  RESET_PWD_PATH: '"/auth/resetPassword"',
  INVITE_PATH: '"/auth/invite"'
})
