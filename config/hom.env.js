var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"homologation"',
  URL_FRONT: '"http://hom.voy.com.br/arquivin/"',
  URL_API: '"http://hom.voy.com.br/arquivin-api"',
  REST_CHECK_TOKEN: '"/validateToken"',
  LOGIN_PATH: '"/auth/login"',
  RECOVER_PATH: '"/auth/recover"'
})
