var utils = require('./utils')
var config = require('../config')
var isHomologation = process.env.NODE_ENV === 'homologation'

module.exports = {
  loaders: utils.cssLoaders({
    sourceMap: isHomologation
      ? config.build_hom.productionSourceMap
      : config.dev.cssSourceMap,
    extract: isHomologation
  }),
  transformToRequire: {
    video: 'src',
    source: 'src',
    img: 'src',
    image: 'xlink:href'
  }
}
